<?php

namespace Dottystyle\LaravelExportsManager\Concerns;

use Illuminate\Contracts\Routing\ResponseFactory;

trait ResponseHelpers
{
    /**
     * @var \Illuminate\Contracts\Routing\ResponseFactory
     */
    protected $responseFactory;

    /**
     * Get the response factory instance.
     * 
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    public function getResponseFactory()
    {
        return $this->responseFactory;
    }

    /**
     * Set the response factory.
     * 
     * @param \Illuminate\Contracts\Routing\ResponseFactory $factory
     * @return static
     */
    public function setResponseFactory(ResponseFactory $factory)
    {   
        $this->responseFactory = $factory;

        return $this;
    }
}