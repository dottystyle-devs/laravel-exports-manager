<?php

namespace Dottystyle\LaravelExportsManager\Concerns;

use Illuminate\Support\Str;

trait ExportManagerHelpers
{
    /**
     * Generate a random file name for generated export files.
     * 
     * @param mixed $exportable 
     * @return string
     */
    protected function generateExportId($exportable)
    {
        return Str::random(40);
    }
}