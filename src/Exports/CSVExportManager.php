<?php

namespace Dottystyle\LaravelExportsManager\Exports;

use Closure;
use Dottystyle\LaravelExportsManager\Exceptions\UnsupportedException;
use League\Csv\Writer;

class CSVExportManager extends FileExportManager
{
    /**
     * @inheritdoc
     */
    protected function makeFileExport($id, $exportable, $fullPath, $relativePath)
    {
        if (! $exportable instanceof CSVExportable) {
            throw new UnsupportedException($exportable);
        }

        $csv = Writer::createFromPath($fullPath, 'w+');
        $csv->insertOne($exportable->csvExportHeaders());

        $items = $exportable->csvExportRows();

        foreach ($items as $index => $row) {
            $csv->insertOne($this->exportRow($row, $index, $exportable));
        }

        unset($csv);

        // System or PHP doesn't return the correct mimetype for CSV so we will force it.
        return new FileExport($id, $fullPath, $this->exportFileMimeType());
    }

    /**
     * Export a row on the exportable.
     * 
     * @param mixed $row
     * @param int $index
     * @param \Dottystyle\LaravelExportsManager\Exports\CSVExportable $exportable
     * @return array
     */
    protected function exportRow($row, $index, $exportable)
    {
        return $exportable->csvExportRow($row, $index);
    }

    /**
     * @inheritdoc
     */
    public function exportFileExtension()
    {
        return 'csv';
    }

    /**
     * @inheritdoc
     */
    public function exportFileMimeType()
    {
        return 'text/csv';
    }

    /**
     * Create a CSV exportable object from a traversable object/data.
     * 
     * @param array $headers
     * @param \Traversable $rows
     * @param \Closure $exportRow (optional)
     * @return \Dottystyle\LaravelExportsManager\Exports\SimpleCSVExportable
     */
    public function createExportableFromTraversable(array $headers, $rows, Closure $exportRow = null)
    {
        return new SimpleCSVExportable($headers, $rows, $exportRow);
    }
}
