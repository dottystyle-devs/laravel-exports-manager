<?php

namespace Dottystyle\LaravelExportsManager\Exports;

use Exception;
use Dottystyle\LaravelExportsManager\ExportManagerInterface;
use Dottystyle\LaravelExportsManager\Concerns\ExportManagerHelpers;
use Dottystyle\LaravelExportsManager\Concerns\ResponseHelpers;
use Dottystyle\LaravelExportsManager\Exceptions\ExportNotFoundException;
use Illuminate\Contracts\Filesystem\Filesystem;

abstract class FileExportManager implements ExportManagerInterface
{
    use ExportManagerHelpers, ResponseHelpers;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $storage;

    /**
     * @var string
     */
    protected $dir;

    /**
     * Create new instance of CSV export whose files reside on one of the storage disk managed by the framework.
     *
     * @param \Illuminate\Contracts\Filesystem\Filesystem $storage
     * @param string $dir
     */
    public function __construct(Filesystem $storage, $dir)
    {
        $this->storage = $storage;
        $this->dir = rtrim($dir, '/');
    }

    /**
     * @inheritdoc
     */
    public function make($exportable)
    {
        try {
            $relativePath = $this->getRelativeExportFilePath(
                $id = $this->generateExportId($exportable)
            );

            return $this->handleExport(
                $this->makeFileExport($id, $exportable, $this->storage->path($relativePath), $relativePath)
            );
        } catch (Exception $e) {
            if (isset($relativePath)) {
                $this->storage->delete($relativePath);
            }

            throw $e;
        }
    }

    /**
     * Make the file export object.
     *
     * @param string $id
     * @param mixed $exportable
     * @param string $fullPath
     * @param string $relativePath
     * @return \Dottystyle\LaravelExportsManager\Exports\FileExport
     */
    abstract protected function makeFileExport($id, $exportable, $fullPath, $relativePath);

    /**
     * @inheritdoc
     */
    final public function get($id)
    {
        $relativePath = $this->getRelativeExportFilePath($id);

        if (! $this->storage->exists($relativePath)) {
            throw new ExportNotFoundException($id);
        }

        return $this->handleExport(
            $this->getFileExport($id, $this->storage->path($relativePath), $relativePath)
        );
    }

    /**
     * Get the file export object with the given export id.
     *
     * @param string $id
     * @param string $fullPath
     * @param string $relativePath
     * @return \Dottystyle\LaravelExportsManager\Exports\FileExport
     */
    protected function getFileExport($id, $fullPath, $relativePath)
    {
        return new FileExport($id, $fullPath, $this->exportFileMimeType());
    }

    /**
     * Get the storage path for the export file.
     *
     * @param mixed $id
     * @return string
     */
    protected function getRelativeExportFilePath($id)
    {
        return "{$this->dir}/$id.".$this->exportFileExtension();
    }

    /**
     * Handle the export object.
     * 
     * @param \Dottystyle\LaravelExportsManager\Exports\FileExport $export
     * @return \Dottystyle\LaravelExportsManager\Exports\FileExport
     */
    protected function handleExport(FileExport $export)
    {
        if ($this->responseFactory) {
            $export->setResponseFactory($this->responseFactory);
        }

        return $export;
    }

    /**
     * @inheritdoc
     */
    public function delete($id)
    {
        $export = $this->get($id);

        $this->storage->delete($export->uri());
    }

    /**
     * Get the file extension of exported files.
     *
     * @return string
     */
    abstract public function exportFileExtension();

    /**
     * Get the mimetype of the exported files.
     *
     * @return string
     */
    abstract public function exportFileMimeType();
}
