<?php

namespace Dottystyle\LaravelExportsManager\Exports;

use Closure;
use Traversable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;

class SimpleCSVExportable implements CSVExportable
{
    /**
     * @var array $headers
     */
    protected $headers;

    /**
     * @var \Traversable
     */
    protected $rows;

    /**
     * @var \Closure
     */
    protected $rowCallback;

    /**
     * @param array $headers
     * @param \Traversable $rows
     * @param \Closure $rowCallback
     */
    public function __construct(array $headers, Traversable $rows, Closure $rowCallback = null)
    {
        $this->headers = $headers;
        $this->rows = $rows;
        $this->rowCallback = $rowCallback;
    }

    /**
     * @inheritdoc
     */
    public function csvExportHeaders()
    {
        return $this->headers;
    }
    
    /**
     * @inheritdoc
     */
    public function csvExportRows()
    {
        return $this->rows;
    }

    /**
     * @inheritdoc
     */
    public function csvExportRow($row, $index)
    {
        if (! $this->rowCallback) {
            if ($row instanceof Arrayable) {
                $row = $row->toArray();
            } elseif (! is_array($row)) {
                return [];
            }

            return Arr::only($row, $this->headers);
        }

        return call_user_func($this->rowCallback, $row, $index);
    }
}