<?php

namespace Dottystyle\LaravelExportsManager\Exports;

use SplFileInfo;
use Carbon\Carbon;
use Dottystyle\LaravelExportsManager\ByteSize;
use Dottystyle\LaravelExportsManager\Downloadable;
use Dottystyle\LaravelExportsManager\Export;
use Dottystyle\LaravelExportsManager\Concerns\ResponseHelpers;
use Illuminate\Support\Facades\File;
use Illuminate\Contracts\Support\Arrayable;

class FileExport implements Arrayable, Downloadable, Export
{
    use ResponseHelpers;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var \SplFileInfo
     */
    protected $info;

    /**
     * @var string
     */
    protected $mimeType;

    /**
     * @var \Carbon\Carbon
     */
    protected $createdAt;

    /**
     * Create new instance of CSV export.
     *
     * @param string $id
     * @param string $path
     * @param string $mimeType (optional)
     * @param \Carbon\Carbon $createdAt (optional)
     */
    public function __construct($id, $path, $mimeType = null, $createdAt = null)
    {
        $this->id = $id;
        $this->info = new SplFileInfo($path);
        $this->mimeType = $mimeType ?? File::mimeType($path);
        $this->createdAt = $createdAt ?? Carbon::now();
    }

    /**
     * @inheritdoc
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function name()
    {
        return $this->info->getBaseName();
    }

    /**
     * Get the extension of the file.
     *
     * @return string
     */
    public function extension()
    {
        return $this->info->getExtension();
    }

    /**
     * Get the mime type of the file.
     * 
     * @return string
     */
    public function mimeType()
    {
        return $this->mimeType;
    }

    /**
     * @inheritdoc
     */
    public function uri()
    {
        return $this->info->getPathname();
    }

    /**
     * @inheritdoc
     */
    public function size()
    {
        return new ByteSize($this->info->getSize());
    }

    /**
     * @inheritdoc
     */
    public function createdAt()
    {
        return $this->createdAt;
    }
    
    /**
     * @inheritdoc
     */
    public function download($name = null, $headers = [])
    {
        return $this->responseFactory->download(
            $this->uri(),
            $name ? "$name.{$this->extension()}" : $this->name(),
            array_merge($headers, [
                'Content-Type' => $this->mimeType()
            ])
        );
    }

    /**
     * Get the array representation of the export.
     * 
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id(),
            'file_name' => $this->name(),
            'file_size' => $this->size()->humanized(),
            'mime_type' => $this->mimeType()
        ];
    }
}
