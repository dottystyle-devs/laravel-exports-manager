<?php

namespace Dottystyle\LaravelExportsManager\Exports;

interface CSVExportable
{
    /**
     * Get the headers to be used on the exported CSV file.
     * 
     * @return array
     */
    public function csvExportHeaders();
    
    /**
     * Get the rows to be exported.
     * 
     * @return \Traversable
     */
    public function csvExportRows();

    /**
     * Get the data to be exported for a row.
     * 
     * @param mixed $row
     * @param int $index
     * @return array
     */
    public function csvExportRow($row, $index);
}