<?php

namespace Dottystyle\LaravelExportsManager;

interface Export
{
    /**
     * Get the unique identifier of the export.
     * 
     * @return mixed
     */
    public function id();

    /**
     * Get the export name.
     *
     * @return string
     */
    public function name();

    /**
     * Get the URI of the export.
     * 
     * @return string
     */
    public function uri();

    /**
     * Get the size of the export.
     * 
     * @return \Dottystyle\LaravelExportsManager\ExportSize
     */
    public function size();

    /**
     * Get the date the export was created.
     * 
     * @return \Carbon\Carbon
     */
    public function createdAt();
}
