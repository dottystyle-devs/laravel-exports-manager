<?php

namespace Dottystyle\LaravelExportsManager;

interface ExportSize
{
    /**
     * Format the export size in a human-readable format.
     * 
     * @return string
     */
    public function humanized();
}