<?php

namespace Dottystyle\LaravelExportsManager;

use Symfony\Component\HttpFoundation\BinaryFileResponse;

interface Downloadable
{
    /**
     * Download an export.
     */
    public function download($name = null, $headers = []);
}