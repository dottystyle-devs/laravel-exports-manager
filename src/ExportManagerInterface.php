<?php

namespace Dottystyle\LaravelExportsManager;

interface ExportManagerInterface
{
    /**
     * Write the exported data to the CSV file.
     *
     * @param mixed $exportable
     * @return \Dottystyle\LaravelExportsManager\Export
     */
    public function make($exportable);

    /**
     * Load the export object with the given export id.
     *
     * @param mixed $id
     * @return \Dottystyle\LaravelExportsManager\Export
     *
     * @throws \Dottystyle\LaravelExportsManager\Exceptions\ExportNotFoundException
     */
    public function get($id);

    /**
     * Delete an export by its id.
     * 
     * @param mixed $id
     * @return void
     * 
     * @throws \Dottystyle\LaravelExportsManager\Exceptions\ExportNotFoundException
     */
    public function delete($id);
}
