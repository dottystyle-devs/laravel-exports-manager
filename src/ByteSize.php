<?php

namespace Dottystyle\LaravelExportsManager;

use ScriptFUSION\Byte\ByteFormatter;
use ScriptFUSION\Byte\Unit\SymbolDecorator;

class ByteSize extends ExportSize
{
    /**
     * @var float
     */
    protected $value;

    /**
     * Create new instance of export size in bytes.
     * 
     * @param int $value
     * @param mixed $SymbolDecorator (optional)
     */
    public function __construct($value, $symbolDecorator = null)
    {
        $this->value = $value;
        $this->decorator = $symbolDecorator ?? SymbolDecorator::SUFFIX_METRIC;
    }

    /**
     * @inheritdoc
     */
    public function humanized()
    {
        return (new ByteFormatter(new SymbolDecorator($this->symbolDecorator)))
            ->format($this->value);
    }
}