<?php

namespace Dottystyle\LaravelExportsManager\Exceptions;

use Exception;

class UnsupportedException extends ExportException
{
    /**
     * @var mixed
     */
    public $exportable;

    /**
     * Create new instance of exception.
     * 
     * @param mixed $exportable
     * @param string $message (optional)
     * @param mixed $code (optional)
     * @param \Exception $previous (optional)
     */
    public function __construct($exportable, $message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message ?? 'Data type to be exported is not supported', $code, $previous);

        $this->exportable = $exportable;
    }
}
