<?php

namespace Dottystyle\LaravelExportsManager\Exceptions;

use Exception;

class ExportException extends Exception
{
}
