<?php

namespace Dottystyle\LaravelExportsManager\Exceptions;

class ExportNotFoundException extends ExportException
{
    /**
     * @var mixed
     */
    public $exportId;

    /**
     * Create new instance of exception.
     * 
     * @param mixed $exportId
     */
    public function __construct($exportId)
    {
        $this->exportId = $exportId;
    }
}
